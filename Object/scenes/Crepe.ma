//Maya ASCII 2020 scene
//Name: Crepe.ma
//Last modified: Thu, Apr 09, 2020 02:14:41 PM
//Codeset: 932
requires maya "2020";
requires "stereoCamera" "10.0";
requires "mtoa" "4.0.0";
requires "stereoCamera" "10.0";
currentUnit -l meter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2020";
fileInfo "version" "2020";
fileInfo "cutIdentifier" "201911140446-42a737a01c";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 18363)\n";
fileInfo "UUID" "7B2C1C3D-4C59-0630-D4A7-F6AB56F6D781";
createNode transform -s -n "persp";
	rename -uid "20F222F6-4886-A3D3-CF0F-5D9FBAFE0416";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.12557820126203539 1.7854297926854961 1.1493139079928951 ;
	setAttr ".r" -type "double3" 302.06164727004756 -6.6000000000008621 2.0011084909432195e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "E2E62F2E-444E-3067-8C41-DF9A9F3D5274";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 2.1762850418949005;
	setAttr ".ow" 0.1;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 1.1444091796875e-05 -8.8009983301596401e-08 0 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "5FC3228A-4B2D-07B1-602C-9FBACC22305E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.047076198362272691 16.337800093263812 0.053242150099132371 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "8D63D11C-4D55-0B85-2AB3-E4BF755145D3";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 16.337800093259158;
	setAttr ".ow" 2.7021221937672837;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 32.322338104248047 4.6566128730773926e-10 14.016490936279297 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "ED00249C-46DB-F056-12BC-5B8150624E49";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.3232233810424805 4.6566128730773927e-12 10.982873026280783 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "65B2A44F-4F44-E43D-7256-D9AD23A8DF3E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 10.84270811691799;
	setAttr ".ow" 1.4247928820158309;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 32.322338104248047 4.6566128730773926e-10 14.016490936279297 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "69FADF6C-4CE7-5E17-F7D1-199DB2EFEE3C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 10.068724624944265 3.4924596548080443e-12 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "274BE7CF-4654-34F5-DC48-32890B6E1823";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 10.068724510503346;
	setAttr ".ow" 0.045585026933156587;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 1.1444091796875e-05 3.4924596548080444e-10 0 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode joint -n "Root";
	rename -uid "766DBD66-46AB-46C0-3802-E5BD8726AD9B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".radi" 2;
createNode joint -n "Up" -p "Root";
	rename -uid "D0D48EA2-4883-BB20-4D97-0F8FF1886242";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".radi" 2;
createNode joint -n "Up01" -p "Up";
	rename -uid "9C228913-4E30-4CB1-6DF5-46B066A035F1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".r" -type "double3" 0 -22.5 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -89.999999999999986 0 ;
	setAttr ".bps" -type "matrix" -0.38268343236508939 0 0.92387953251128696 0 0 1 0 0
		 -0.92387953251128696 -0 -0.38268343236508939 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Up01";
	rename -uid "62E70014-4B6A-6969-7BEE-F4B32A98D559";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Up|Up01|locator1";
	rename -uid "989367E1-45D1-8F3C-9B3F-AF9871F57251";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Up02" -p "Up";
	rename -uid "0E7412EB-4BB4-34E7-F079-A3AB7165520B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".r" -type "double3" 0 -45 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -89.999999999999986 0 ;
	setAttr ".bps" -type "matrix" -0.70710678118654746 0 0.70710678118654779 0 0 1 0 0
		 -0.70710678118654779 -0 -0.70710678118654746 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Up02";
	rename -uid "BC2D59DB-4C2C-C8AA-E78A-E6B160BFC8CB";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Up|Up02|locator1";
	rename -uid "02600D63-42D5-9DD0-BD4B-079676EC0B92";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Up03" -p "Up";
	rename -uid "D1294E45-4D79-ADFD-B4BA-31AB96DAB481";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".r" -type "double3" 0 -67.5 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -89.999999999999986 0 ;
	setAttr ".bps" -type "matrix" -0.92387953251128674 0 0.38268343236509028 0 0 1 0 0
		 -0.38268343236509028 -0 -0.92387953251128674 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Up03";
	rename -uid "BE340FD3-46B8-6392-DE16-ED87FF2C7C7D";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Up|Up03|locator1";
	rename -uid "E518DD20-4175-F637-C2DF-7A8FC162CC75";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Up04" -p "Up";
	rename -uid "4DE100FD-4018-0DDC-D269-46BB8067AB5A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -89.999999999999986 0 ;
	setAttr ".bps" -type "matrix" -1 0 2.2204460492503131e-16 0 0 1 0 0 -2.2204460492503131e-16 -0 -1 0
		 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Up04";
	rename -uid "DBBDAAFB-485A-B23E-F52B-AC9D3BE09C96";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Up|Up04|locator1";
	rename -uid "439C7655-4CE0-CFBE-EE9F-6B9863ADD58A";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Up05" -p "Up";
	rename -uid "9D47D563-4A08-4603-60E3-3FBBB45B0E4E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".r" -type "double3" 0 -22.5 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".bps" -type "matrix" -0.92387953251128674 0 -0.38268343236508984 0 0 1 0 0
		 0.38268343236508984 0 -0.92387953251128674 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Up05";
	rename -uid "DA017C6B-43CE-92F9-A7D0-E49E75B1E514";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Up|Up05|locator1";
	rename -uid "E154C893-4F3B-9679-DDD3-32BBB99F6696";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Up06" -p "Up";
	rename -uid "A776D54F-45F1-1917-CE05-1BBFD3D0B5AD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".r" -type "double3" 0 -45 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".bps" -type "matrix" -0.70710678118654746 0 -0.70710678118654768 0 0 1 0 0
		 0.70710678118654768 0 -0.70710678118654746 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Up06";
	rename -uid "F6C5C493-4FF5-316D-68A6-3A86DE6841A5";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Up|Up06|locator1";
	rename -uid "F53EC358-4352-2085-0915-CD8B66821CBF";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Up07" -p "Up";
	rename -uid "41E302D7-4EE1-5037-8C71-98B0462010D2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".r" -type "double3" 0 -67.5 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".bps" -type "matrix" -0.38268343236508984 0 -0.92387953251128663 0 0 1 0 0
		 0.92387953251128663 0 -0.38268343236508984 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Up07";
	rename -uid "C6ED17E5-4595-33F2-B61A-6998A610712E";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Up|Up07|locator1";
	rename -uid "494C45C2-4AA0-F42C-D116-00ACAEFB35BB";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Low" -p "Root";
	rename -uid "D03460A0-40AD-48BB-296A-F39B728124AD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".radi" 2;
createNode joint -n "Low01" -p "Low";
	rename -uid "830A71D1-4B59-6474-8AD0-239DBBE31E6E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".r" -type "double3" 0 -67.5 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.38268343236508984 0 0.92387953251128663 0 0 1 0 0
		 -0.92387953251128663 0 0.38268343236508984 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Low01";
	rename -uid "18D2CF1E-4327-1086-EDC2-DB9FB5ADC8FC";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Low|Low01|locator1";
	rename -uid "900C457D-4E69-79D5-AF53-3D9D93405855";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Low02" -p "Low";
	rename -uid "B9B1E881-4718-A243-9FC0-8DA25037239A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".r" -type "double3" 0 -45 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.70710678118654746 0 0.70710678118654757 0 0 1 0 0
		 -0.70710678118654757 0 0.70710678118654746 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Low02";
	rename -uid "F248B218-40C4-4D4E-2DB0-A58022E38DEF";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Low|Low02|locator1";
	rename -uid "CED576CE-434D-E94E-B4BB-FCB966031206";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Low03" -p "Low";
	rename -uid "5ABDD2BF-4CC3-3B1D-F244-7889B27404BB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".r" -type "double3" 0 -22.5 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.92387953251128674 0 0.38268343236508973 0 0 1 0 0
		 -0.38268343236508973 0 0.92387953251128674 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Low03";
	rename -uid "AF0A9068-46A2-8ACE-4E1F-B998E773D015";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Low|Low03|locator1";
	rename -uid "C0B6CC45-481C-FCB9-5EA2-63979D8E6CD9";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Low04" -p "Low";
	rename -uid "E580210C-45A1-37EB-E940-D898D3E9F195";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Low04";
	rename -uid "664759AB-4B80-0E8D-E0BD-A18BDFD43E94";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Low|Low04|locator1";
	rename -uid "AA52F12B-4FD6-5253-5468-CFB49135000C";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Low05" -p "Low";
	rename -uid "64AF6900-437A-A841-A38A-C0ACF1A8349B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".r" -type "double3" 0 -67.5 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 90 0 ;
	setAttr ".bps" -type "matrix" 0.92387953251128674 0 -0.38268343236508995 0 0 1 0 0
		 0.38268343236508995 0 0.92387953251128674 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Low05";
	rename -uid "4B4D9110-4C0D-E9BA-1FDE-5D91979BCB23";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Low|Low05|locator1";
	rename -uid "D587203F-47E1-9477-3777-F39C301ED0BD";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Low06" -p "Low";
	rename -uid "3D2B8132-43CE-1E87-565F-1B9B2A95A9FF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".r" -type "double3" 0 -45 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 90 0 ;
	setAttr ".bps" -type "matrix" 0.70710678118654746 0 -0.70710678118654757 0 0 1 0 0
		 0.70710678118654757 0 0.70710678118654746 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Low06";
	rename -uid "CB8C668A-4B5E-425F-67CF-2D9CA8F177AB";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Low|Low06|locator1";
	rename -uid "ACA1E08E-4AF7-DDB0-CACD-90B8A7705CA4";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Low07" -p "Low";
	rename -uid "15367A1D-4D06-8B9E-C0D0-088F97CB86D5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".r" -type "double3" 0 -22.5 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 90 0 ;
	setAttr ".bps" -type "matrix" 0.38268343236508962 0 -0.92387953251128685 0 0 1 0 0
		 0.92387953251128685 0 0.38268343236508962 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Low07";
	rename -uid "D1C7E0F3-44E2-4CA7-8C3C-8194B85E0BA3";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Low|Low07|locator1";
	rename -uid "6A551B4A-4E0A-2718-C1F4-5D912D0FE12D";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Left" -p "Root";
	rename -uid "631A5081-474B-72E0-0E73-6790BB6BA89C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".r" -type "double3" 0 -90 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0 0 1 0 0 1 0 0 -1 0 0 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Left";
	rename -uid "2E72669D-42CF-7C18-AC45-DF920E493116";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Left|locator1";
	rename -uid "3AA32EE0-4E39-9795-7C24-3C9DA8ADA989";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode joint -n "Right" -p "Root";
	rename -uid "02F9777B-438C-8FCC-82FC-B0A80CDA8AA1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0 0 -1 0 0 1 0 0 1 0 0 0 0 0 0 1;
	setAttr ".radi" 2;
createNode transform -n "locator1" -p "Right";
	rename -uid "EA788EEC-4ADD-CD87-4BC9-5BA1B9DE861E";
	setAttr ".t" -type "double3" 0 0 0.2094869346243364 ;
createNode locator -n "locatorShape1" -p "|Root|Right|locator1";
	rename -uid "31F855BE-4ACB-1C10-D703-0C9D69F8BE3F";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.001 0.001 0.001 ;
createNode transform -n "CrepeUp";
	rename -uid "AA7BCF52-430E-6DEC-9657-8881C3364B84";
	setAttr ".t" -type "double3" 0 2.5000001769512892e-05 0 ;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr ".s" -type "double3" 0.5 0.5 0.5 ;
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "CrepeUpShape" -p "CrepeUp";
	rename -uid "C15457DC-4BC0-29BD-8235-478644891D2C";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".db" yes;
	setAttr ".dmb" yes;
	setAttr ".bw" 3;
	setAttr ".ns" 2;
	setAttr ".ndt" 2;
	setAttr ".dn" yes;
	setAttr ".difs" yes;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "CrepeUpShapeOrig" -p "CrepeUp";
	rename -uid "0AAA5979-4D8C-6315-31D5-87AEB32C07CD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 83 ".vt[0:82]"  0.92387938 4.6566128e-12 -0.38268298 0.70710695 4.6566128e-12 -0.70710635
		 0.38268372 4.6566128e-12 -0.92387915 3.9756299e-07 4.6566128e-12 -0.9999997 -0.38268298 4.6566128e-12 -0.92387938
		 -0.70710635 4.6566128e-12 -0.70710671 -0.92387909 4.6566128e-12 -0.38268352 -0.9999997 4.6566128e-12 0
		 1.000000119209 4.6566128e-12 0 0.69290954 4.6566128e-12 -0.28701222 0.53033018 4.6566128e-12 -0.53032976
		 0.28701282 4.6566128e-12 -0.69290942 3.2678247e-07 4.6566128e-12 -0.74999976 -0.28701222 4.6566128e-12 -0.69290954
		 -0.53032976 4.6566128e-12 -0.53033006 -0.69290942 4.6566128e-12 -0.28701264 -0.7499997 4.6566128e-12 0
		 0.75000018 4.6566128e-12 0 0.46193972 4.6566128e-12 -0.19134149 0.3535535 4.6566128e-12 -0.35355318
		 0.19134192 4.6566128e-12 -0.46193957 2.5600195e-07 4.6566128e-12 -0.49999985 -0.19134144 4.6566128e-12 -0.46193969
		 -0.35355312 4.6566128e-12 -0.35355335 -0.46193951 4.6566128e-12 -0.19134176 -0.49999976 4.6566128e-12 0
		 0.50000012 4.6566128e-12 -5.551115e-19 0.24879664 4.6566128e-12 -0.024504285 0.23096992 4.6566128e-12 -0.095670745
		 0.17677681 4.6566128e-12 -0.17677659 0.095671013 4.6566128e-12 -0.23096979 1.8522144e-07 4.6566128e-12 -0.24999993
		 -0.095670663 4.6566128e-12 -0.23096985 -0.1767765 4.6566128e-12 -0.17677668 -0.2309697 4.6566128e-12 -0.095670879
		 -0.248796 4.6566128e-12 -0.024504326 -0.24999973 4.6566128e-12 5.9604646e-09 0.25000012 4.6566128e-12 -1.1564821e-18
		 1.5258789e-07 4.6566128e-12 -0.024504326 0.92387938 4.6566128e-12 0.38268298 0.70710695 4.6566128e-12 0.70710635
		 0.38268372 4.6566128e-12 0.92387915 3.9756299e-07 4.6566128e-12 0.9999997 -0.38268298 4.6566128e-12 0.92387938
		 -0.70710635 4.6566128e-12 0.70710671 -0.92387909 4.6566128e-12 0.38268352 0.69290954 4.6566128e-12 0.28701222
		 0.53033018 4.6566128e-12 0.53032976 0.28701282 4.6566128e-12 0.69290942 3.2678247e-07 4.6566128e-12 0.74999976
		 -0.28701222 4.6566128e-12 0.69290954 -0.53032976 4.6566128e-12 0.53033006 -0.69290942 4.6566128e-12 0.28701264
		 0.46193972 4.6566128e-12 0.19134149 0.3535535 4.6566128e-12 0.35355318 0.19134192 4.6566128e-12 0.46193957
		 2.5600195e-07 4.6566128e-12 0.49999985 -0.19134144 4.6566128e-12 0.46193969 -0.35355312 4.6566128e-12 0.35355335
		 -0.46193951 4.6566128e-12 0.19134176 0.24879664 4.6566128e-12 0.024504285 0.23096992 4.6566128e-12 0.095670745
		 0.17677681 4.6566128e-12 0.17677659 0.095671013 4.6566128e-12 0.23096979 1.8522144e-07 4.6566128e-12 0.24999993
		 -0.095670663 4.6566128e-12 0.23096985 -0.1767765 4.6566128e-12 0.17677668 -0.2309697 4.6566128e-12 0.095670879
		 -0.248796 4.6566128e-12 0.024504326 1.5258789e-07 4.6566128e-12 0.024504326 1.4305115e-07 4.6566128e-12 -5.7824112e-19
		 -0.99879575 4.6566128e-12 -0.024504326 0.99879622 4.6566128e-12 -0.024504326 -0.74879593 4.6566128e-12 -0.024504328
		 0.74879622 4.6566128e-12 -0.024504326 -0.49879596 4.6566128e-12 -0.024504326 0.49879619 4.6566128e-12 -0.024504326
		 -0.99879575 4.6566128e-12 0.024504326 -0.74879593 4.6566128e-12 0.024504328 0.99879622 4.6566128e-12 0.024504326
		 0.74879622 4.6566128e-12 0.024504326 -0.49879596 4.6566128e-12 0.024504326 0.49879619 4.6566128e-12 0.024504326;
	setAttr -s 166 ".ed[0:165]"  72 0 0 0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0
		 6 71 0 8 72 0 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 73 1 17 74 1 18 19 1
		 19 20 1 20 21 1 21 22 1 22 23 1 23 24 1 24 75 1 26 76 1 28 29 1 29 30 1 30 31 1 31 32 1
		 32 33 1 33 34 1 34 35 1 37 27 1 0 9 1 1 10 1 2 11 1 3 12 1 4 13 1 5 14 1 6 15 1 7 16 0
		 8 17 0 9 18 1 10 19 1 11 20 1 12 21 1 13 22 1 14 23 1 15 24 1 16 25 0 17 26 0 18 28 1
		 19 29 1 20 30 1 21 31 1 22 32 1 23 33 1 24 34 1 25 36 0 26 37 0 27 38 1 28 38 1 29 38 1
		 30 38 1 31 38 1 32 38 1 33 38 1 34 38 1 35 38 1 36 38 0 37 38 0 36 70 0 39 46 1 39 40 0
		 40 47 1 46 47 1 40 41 0 41 48 1 47 48 1 41 42 0 42 49 1 48 49 1 42 43 0 43 50 1 49 50 1
		 43 44 0 44 51 1 50 51 1 44 45 0 45 52 1 51 52 1 45 77 0 52 78 1 8 79 0 17 80 1 46 53 1
		 47 54 1 53 54 1 48 55 1 54 55 1 49 56 1 55 56 1 50 57 1 56 57 1 51 58 1 57 58 1 52 59 1
		 58 59 1 59 81 1 26 82 1 53 61 1 54 62 1 61 62 1 55 63 1 62 63 1 56 64 1 63 64 1 57 65 1
		 64 65 1 58 66 1 65 66 1 59 67 1 66 67 1 67 68 1 68 36 1 37 60 1 60 69 1 61 69 1 62 69 1
		 63 69 1 64 69 1 65 69 1 66 69 1 67 69 1 68 69 1 36 69 0 37 69 0 70 37 0 38 70 1 70 69 1
		 71 7 0 73 16 1 74 9 1 75 25 1 76 18 1 27 28 1 35 36 1 71 73 1 72 74 1 73 75 1 74 76 1
		 75 35 1 76 27 1 77 7 0 78 16 1 79 39 0 80 46 1 81 25 1 82 53 1 60 61 1 78 77 1 80 79 1
		 81 78 1 82 80 1 68 81 1 60 82 1;
	setAttr -s 84 -ch 312 ".fc[0:83]" -type "polyFaces" 
		f 4 -34 1 34 -10
		f 4 -35 2 35 -11
		f 4 -36 3 36 -12
		f 4 -37 4 37 -13
		f 4 -38 5 38 -14
		f 4 -39 6 39 -15
		f 4 -40 7 147 -16
		f 4 8 148 -17 -42
		f 4 -43 9 43 -18
		f 4 -44 10 44 -19
		f 4 -45 11 45 -20
		f 4 -46 12 46 -21
		f 4 -47 13 47 -22
		f 4 -48 14 48 -23
		f 4 -49 15 149 -24
		f 4 16 150 -25 -51
		f 4 -52 17 52 -26
		f 4 -53 18 53 -27
		f 4 -54 19 54 -28
		f 4 -55 20 55 -29
		f 4 -56 21 56 -30
		f 4 -57 22 57 -31
		f 4 -58 23 151 -32
		f 4 24 152 -33 -60
		f 3 -62 25 62
		f 3 -63 26 63
		f 3 -64 27 64
		f 3 -65 28 65
		f 3 -66 29 66
		f 3 -67 30 67
		f 3 -68 31 68
		f 3 146 69 -69
		f 3 32 60 -71
		f 3 138 137 70
		f 4 -73 -156 -162 156
		f 4 75 -75 -74 72
		f 4 78 -78 -77 74
		f 4 81 -81 -80 77
		f 4 84 -84 -83 80
		f 4 87 -87 -86 83
		f 4 90 -90 -89 86
		f 4 92 160 -92 89
		f 4 41 94 161 -94
		f 4 -96 -157 -164 158
		f 4 97 -97 -76 95
		f 4 99 -99 -79 96
		f 4 101 -101 -82 98
		f 4 103 -103 -85 100
		f 4 105 -105 -88 102
		f 4 107 -107 -91 104
		f 4 108 162 -93 106
		f 4 50 109 163 -95
		f 4 -111 -159 -166 159
		f 4 112 -112 -98 110
		f 4 114 -114 -100 111
		f 4 116 -116 -102 113
		f 4 118 -118 -104 115
		f 4 120 -120 -106 117
		f 4 122 -122 -108 119
		f 4 123 164 -109 121
		f 4 59 125 165 -110
		f 3 -128 -160 126
		f 3 -129 -113 127
		f 3 -130 -115 128
		f 3 -131 -117 129
		f 3 -132 -119 130
		f 3 -133 -121 131
		f 3 -134 -123 132
		f 3 -135 -124 133
		f 3 134 -136 -125
		f 3 136 -127 -126
		f 3 -137 -138 139
		f 3 -70 71 -139
		f 3 -140 -72 135
		f 4 -148 140 40 -142
		f 4 -143 -149 0 33
		f 4 -150 141 49 -144
		f 4 -145 -151 142 42
		f 4 -152 143 58 -147
		f 4 -146 -153 144 51
		f 3 -61 145 61
		f 4 -161 154 -41 -154
		f 4 -163 157 -50 -155
		f 4 -165 124 -59 -158;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".dmb" yes;
	setAttr ".bw" 3;
	setAttr ".ns" 2;
	setAttr ".ndt" 2;
	setAttr ".dn" yes;
	setAttr ".difs" yes;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "CrepeBack";
	rename -uid "AA83D39B-4789-B0F8-B950-3E89450B91F9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 2.5000001769512892e-05 0 ;
	setAttr ".s" -type "double3" 0.5 0.5 0.5 ;
createNode mesh -n "CrepeBackShape" -p "CrepeBack";
	rename -uid "2AD33B30-4612-B048-9C8E-969AC59182B6";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:83]";
	setAttr ".mb" no;
	setAttr ".csh" no;
	setAttr ".rcsh" no;
	setAttr ".vis" no;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 83 ".vt[0:82]"  0.92387938 -0.000099999997 -0.38268298 0.70710695 -0.000099999997 -0.70710635
		 0.38268372 -0.000099999997 -0.92387915 3.9756299e-07 -0.000099999997 -0.9999997 -0.38268298 -0.000099999997 -0.92387938
		 -0.70710635 -0.000099999997 -0.70710671 -0.92387909 -0.000099999997 -0.38268352 -0.9999997 -0.000099999997 0
		 1.000000119209 -0.000099999997 0 0.69290954 -0.000099999997 -0.28701222 0.53033018 -0.000099999997 -0.53032976
		 0.28701282 -0.000099999997 -0.69290942 3.2678247e-07 -0.000099999997 -0.74999976
		 -0.28701222 -0.000099999997 -0.69290954 -0.53032976 -0.000099999997 -0.53033006 -0.69290942 -0.000099999997 -0.28701264
		 -0.7499997 -0.000099999997 0 0.75000018 -0.000099999997 0 0.46193972 -0.000099999997 -0.19134149
		 0.3535535 -0.000099999997 -0.35355318 0.19134192 -0.000099999997 -0.46193957 2.5600195e-07 -0.000099999997 -0.49999985
		 -0.19134144 -0.000099999997 -0.46193969 -0.35355312 -0.000099999997 -0.35355335 -0.46193951 -0.000099999997 -0.19134176
		 -0.49999976 -0.000099999997 0 0.50000012 -0.000099999997 -5.551115e-19 0.24879664 -0.000099999997 -0.024504285
		 0.23096992 -0.000099999997 -0.095670745 0.17677681 -0.000099999997 -0.17677659 0.095671013 -0.000099999997 -0.23096979
		 1.8522144e-07 -0.000099999997 -0.24999993 -0.095670663 -0.000099999997 -0.23096985
		 -0.1767765 -0.000099999997 -0.17677668 -0.2309697 -0.000099999997 -0.095670879 -0.248796 -0.000099999997 -0.024504326
		 -0.24999981 -9.9999976e-05 1.1920929e-09 0.25000012 -9.9999976e-05 -7.864079e-19
		 1.5258789e-07 -9.9999976e-05 -0.024504326 0.92387938 -0.000099999997 0.38268298 0.70710695 -0.000099999997 0.70710635
		 0.38268372 -0.000099999997 0.92387915 3.9756299e-07 -0.000099999997 0.9999997 -0.38268298 -0.000099999997 0.92387938
		 -0.70710635 -0.000099999997 0.70710671 -0.92387909 -0.000099999997 0.38268352 0.69290954 -0.000099999997 0.28701222
		 0.53033018 -0.000099999997 0.53032976 0.28701282 -0.000099999997 0.69290942 3.2678247e-07 -0.000099999997 0.74999976
		 -0.28701222 -0.000099999997 0.69290954 -0.53032976 -0.000099999997 0.53033006 -0.69290942 -0.000099999997 0.28701264
		 0.46193972 -0.000099999997 0.19134149 0.3535535 -0.000099999997 0.35355318 0.19134192 -0.000099999997 0.46193957
		 2.5600195e-07 -0.000099999997 0.49999985 -0.19134144 -0.000099999997 0.46193969 -0.35355312 -0.000099999997 0.35355335
		 -0.46193951 -0.000099999997 0.19134176 0.24879664 -0.000099999997 0.024504285 0.23096992 -0.000099999997 0.095670745
		 0.17677681 -0.000099999997 0.17677659 0.095671013 -0.000099999997 0.23096979 1.8522144e-07 -0.000099999997 0.24999993
		 -0.095670663 -0.000099999997 0.23096985 -0.1767765 -0.000099999997 0.17677668 -0.2309697 -0.000099999997 0.095670879
		 -0.248796 -0.000099999997 0.024504326 1.5258789e-07 -9.9999976e-05 0.024504326 1.4305115e-07 -0.000099999997 -5.7824107e-19
		 -0.99879575 -0.000099999997 -0.024504326 0.99879622 -0.000099999997 -0.024504326
		 -0.74879593 -0.000099999997 -0.024504328 0.74879622 -0.000099999997 -0.024504326
		 -0.49879596 -0.000099999997 -0.024504326 0.49879619 -0.000099999997 -0.024504326
		 -0.99879575 -0.000099999997 0.024504326 -0.74879593 -0.000099999997 0.024504328 0.99879622 -0.000099999997 0.024504326
		 0.74879622 -0.000099999997 0.024504326 -0.49879596 -0.000099999997 0.024504326 0.49879619 -0.000099999997 0.024504326;
	setAttr -s 166 ".ed[0:165]"  0 9 1 0 1 0 1 10 1 9 10 1 1 2 0 2 11 1 10 11 1
		 2 3 0 3 12 1 11 12 1 3 4 0 4 13 1 12 13 1 4 5 0 5 14 1 13 14 1 5 6 0 6 15 1 14 15 1
		 6 71 0 71 73 1 15 73 1 8 72 0 72 74 1 17 74 1 8 17 0 9 18 1 10 19 1 18 19 1 11 20 1
		 19 20 1 12 21 1 20 21 1 13 22 1 21 22 1 14 23 1 22 23 1 15 24 1 23 24 1 73 75 1 24 75 1
		 74 76 1 26 76 1 17 26 0 18 28 1 27 28 1 19 29 1 28 29 1 20 30 1 29 30 1 21 31 1 30 31 1
		 22 32 1 31 32 1 23 33 1 32 33 1 24 34 1 33 34 1 34 35 1 75 35 1 76 27 1 37 27 1 26 37 0
		 28 38 1 29 38 1 30 38 1 31 38 1 32 38 1 33 38 1 34 38 1 35 38 1 35 36 1 36 38 0 27 38 1
		 37 38 0 38 70 1 70 37 0 39 46 1 79 39 0 46 47 1 40 47 1 39 40 0 47 48 1 41 48 1 40 41 0
		 48 49 1 42 49 1 41 42 0 49 50 1 43 50 1 42 43 0 50 51 1 44 51 1 43 44 0 51 52 1 45 52 1
		 44 45 0 52 78 1 78 77 1 45 77 0 17 80 1 80 79 1 8 79 0 46 53 1 53 54 1 47 54 1 54 55 1
		 48 55 1 55 56 1 49 56 1 56 57 1 50 57 1 57 58 1 51 58 1 58 59 1 52 59 1 59 81 1 81 78 1
		 26 82 1 82 80 1 60 61 1 53 61 1 61 62 1 54 62 1 62 63 1 55 63 1 63 64 1 56 64 1 64 65 1
		 57 65 1 65 66 1 58 66 1 66 67 1 59 67 1 67 68 1 68 81 1 37 60 1 60 82 1 61 69 1 62 69 1
		 63 69 1 64 69 1 65 69 1 66 69 1 67 69 1 68 69 1 36 69 0 68 36 1 37 69 0 60 69 1 70 69 1
		 36 70 0 71 7 0 7 16 0 73 16 1 72 0 0 74 9 1 16 25 0 75 25 1 76 18 1 25 36 0 78 16 1
		 77 7 0 80 46 1 81 25 1 82 53 1;
	setAttr -s 84 -ch 312 ".fc[0:83]" -type "polyFaces" 
		f 4 -1 -156 23 156
		f 4 3 -3 -2 0
		f 4 6 -6 -5 2
		f 4 9 -9 -8 5
		f 4 12 -12 -11 8
		f 4 15 -15 -14 11
		f 4 18 -18 -17 14
		f 4 21 -21 -20 17
		f 4 25 24 -24 -23
		f 4 -27 -157 41 159
		f 4 28 -28 -4 26
		f 4 30 -30 -7 27
		f 4 32 -32 -10 29
		f 4 34 -34 -13 31
		f 4 36 -36 -16 33
		f 4 38 -38 -19 35
		f 4 40 -40 -22 37
		f 4 43 42 -42 -25
		f 4 45 -45 -160 60
		f 4 47 -47 -29 44
		f 4 49 -49 -31 46
		f 4 51 -51 -33 48
		f 4 53 -53 -35 50
		f 4 55 -55 -37 52
		f 4 57 -57 -39 54
		f 4 58 -60 -41 56
		f 4 62 61 -61 -43
		f 3 -64 -46 73
		f 3 -65 -48 63
		f 3 -66 -50 64
		f 3 -67 -52 65
		f 3 -68 -54 66
		f 3 -69 -56 67
		f 3 -70 -58 68
		f 3 -71 -59 69
		f 3 70 -73 -72
		f 3 74 -74 -62
		f 3 -75 -77 -76
		f 4 -78 81 80 -80
		f 4 -81 84 83 -83
		f 4 -84 87 86 -86
		f 4 -87 90 89 -89
		f 4 -90 93 92 -92
		f 4 -93 96 95 -95
		f 4 -96 99 -99 -98
		f 4 102 -102 -101 -26
		f 4 -104 79 105 -105
		f 4 -106 82 107 -107
		f 4 -108 85 109 -109
		f 4 -110 88 111 -111
		f 4 -112 91 113 -113
		f 4 -114 94 115 -115
		f 4 -116 97 -118 -117
		f 4 100 -120 -119 -44
		f 4 -122 104 123 -123
		f 4 -124 106 125 -125
		f 4 -126 108 127 -127
		f 4 -128 110 129 -129
		f 4 -130 112 131 -131
		f 4 -132 114 133 -133
		f 4 -134 116 -136 -135
		f 4 118 -138 -137 -63
		f 3 -139 122 139
		f 3 -140 124 140
		f 3 -141 126 141
		f 3 -142 128 142
		f 3 -143 130 143
		f 3 -144 132 144
		f 3 -145 134 145
		f 3 147 146 -146
		f 3 136 149 -149
		f 3 -151 76 148
		f 3 75 -152 72
		f 3 -147 151 150
		f 4 154 -154 -153 20
		f 4 158 -158 -155 39
		f 4 71 -161 -159 59
		f 4 162 153 -162 98
		f 4 -164 101 78 77
		f 4 161 157 -165 117
		f 4 -166 119 163 103
		f 4 164 160 -148 135
		f 4 137 165 121 -121
		f 3 -150 120 138;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".dmb" yes;
	setAttr ".bw" 3;
	setAttr ".ns" 2;
	setAttr ".ndt" 2;
	setAttr ".dn" yes;
	setAttr ".difs" yes;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "DC5EA7E4-439F-004E-FFF3-2AA65B3D75CE";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "52556B15-420A-45CF-02C6-F087C772A165";
	setAttr ".bsdt[0].bscd" -type "Int32Array" 0 ;
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "D427D32B-48B3-917C-110A-5C868D8D59DD";
createNode displayLayerManager -n "layerManager";
	rename -uid "66928A1A-405D-9F71-F24C-639355645C4F";
	setAttr -s 3 ".dli[1:2]"  1 2;
createNode displayLayer -n "defaultLayer";
	rename -uid "53359C42-4BA9-33F6-3C2A-58B6FB25D284";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "61A4F710-47AB-A513-644F-A8AFD3D53822";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "5E7AFB76-4D34-58B6-2A3E-1EADA9DD061E";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "DC9861AD-4DC6-8057-9EFE-70B5A101D6ED";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 782\n            -height 350\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 781\n            -height 350\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 782\n            -height 350\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 1\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1570\n            -height 744\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n"
		+ "            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n"
		+ "                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -selectionOrder \"display\" \n                -expandAttribute 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n"
		+ "                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 0\n                -outliner \"graphEditor1OutlineEd\" \n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n"
		+ "                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n"
		+ "                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n"
		+ "            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n"
		+ "                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n"
		+ "                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n"
		+ "                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n"
		+ "                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 1\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1570\\n    -height 744\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 1\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1570\\n    -height 744\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 1 -size 12 -divisions 2 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "D4782D36-4A45-BF55-53F8-C3AE71E452DF";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 120 -ast 0 -aet 200 ";
	setAttr ".st" 6;
createNode groupId -n "groupId29";
	rename -uid "F7235E08-4ACA-2D28-8429-1EBB84CA8C43";
	setAttr ".ihi" 0;
createNode skinCluster -n "skinCluster1";
	rename -uid "54045AC6-4A7D-4161-373B-E485051480E5";
	setAttr -s 83 ".wl";
	setAttr ".wl[0:82].w"
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25000000000000033 2 0.24999999999999989 3 0.24999999999999989 
		4 0.24999999999999989
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		1 29 1
		1 28 1
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		1 29 1
		1 28 1
		4 1 0.25000000000000033 2 0.24999999999999989 3 0.24999999999999989 
		4 0.24999999999999989
		4 1 0.25000000000000033 2 0.24999999999999989 3 0.24999999999999989 
		4 0.24999999999999989
		4 1 0.25000000000000033 2 0.24999999999999989 3 0.24999999999999989 
		4 0.24999999999999989
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		1 29 1
		1 28 1
		1 28 1
		4 1 0.25000000000000033 2 0.24999999999999989 3 0.24999999999999989 
		4 0.24999999999999989
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		4 1 0.25 2 0.25 3 0.25 4 0.25
		1 29 1
		1 29 1
		1 28 1
		15 0 0.066666666666666471 1 0.066666666666666693 2 0.066666666666666693 
		3 0.066666666666666693 4 0.066666666666666693 19 0.066666666666666693 
		20 0.066666666666666693 21 0.066666666666666693 22 0.066666666666666693 
		23 0.066666666666666693 24 0.066666666666666693 26 0.066666666666666693 
		27 0.066666666666666693 28 0.066666666666666693 29 0.066666666666666693
		1 26 1
		1 27 1
		1 24 1
		1 23 1
		1 22 1
		1 21 1
		1 20 1
		1 26 1
		1 27 1
		1 24 1
		1 23 1
		1 22 1
		1 21 1
		1 20 1
		1 26 1
		1 27 1
		1 24 1
		1 23 1
		1 22 1
		1 21 1
		1 20 1
		1 28 1
		1 26 1
		1 27 1
		1 24 1
		1 23 1
		1 22 1
		1 21 1
		1 20 1
		1 29 1
		15 0 0.066666666666666471 1 0.066666666666666693 2 0.066666666666666693 
		3 0.066666666666666693 4 0.066666666666666693 19 0.066666666666666693 
		20 0.066666666666666693 21 0.066666666666666693 22 0.066666666666666693 
		23 0.066666666666666693 24 0.066666666666666693 26 0.066666666666666693 
		27 0.066666666666666693 28 0.066666666666666693 29 0.066666666666666693
		15 0 0.066666666666666791 1 0.06666666666666668 2 0.06666666666666668 
		3 0.06666666666666668 4 0.06666666666666668 19 0.06666666666666668 
		20 0.06666666666666668 21 0.06666666666666668 22 0.06666666666666668 
		23 0.06666666666666668 24 0.06666666666666668 26 0.06666666666666668 
		27 0.06666666666666668 28 0.06666666666666668 29 0.06666666666666668
		1 29 1
		1 28 1
		1 29 1
		1 28 1
		1 29 1
		1 28 1
		1 29 1
		1 29 1
		1 28 1
		1 28 1
		1 29 1
		1 28 1;
	setAttr -s 30 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
	setAttr ".pm[1]" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
	setAttr ".pm[2]" -type "matrix" -0.38268343236508939 -0 -0.92387953251128696 -0 -0 1 -0 0
		 0.92387953251128696 -0 -0.38268343236508939 -0 -0 0 -0 1;
	setAttr ".pm[3]" -type "matrix" -0.70710678118654735 -0 -0.70710678118654768 -0 -0 1 -0 0
		 0.70710678118654768 -0 -0.70710678118654735 -0 -0 0 -0 1;
	setAttr ".pm[4]" -type "matrix" -0.92387953251128629 -0 -0.38268343236509011 -0 -0 1 -0 0
		 0.38268343236509011 -0 -0.92387953251128629 -0 -0 0 -0 1;
	setAttr ".pm[5]" -type "matrix" -1 -0 -2.2204460492503131e-16 -0 -0 1 -0 0 2.2204460492503131e-16 -0 -1 -0
		 -0 0 -0 1;
	setAttr ".pm[6]" -type "matrix" -0.92387953251128674 -0 0.38268343236508984 -0 -0 1 -0 0
		 -0.38268343236508984 -0 -0.92387953251128674 0 -0 -0 0 1;
	setAttr ".pm[7]" -type "matrix" -0.70710678118654746 -0 0.70710678118654768 -0 -0 1 -0 0
		 -0.70710678118654768 -0 -0.70710678118654746 0 -0 -0 0 1;
	setAttr ".pm[8]" -type "matrix" -0.38268343236508995 -0 0.92387953251128685 -0 -0 1 -0 0
		 -0.92387953251128685 -0 -0.38268343236508995 0 -0 -0 0 1;
	setAttr ".pm[9]" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
	setAttr ".pm[10]" -type "matrix" 0.38268343236508995 -0 -0.92387953251128685 -0 -0 1 -0 0
		 0.92387953251128685 -0 0.38268343236508995 -0 -0 0 -0 1;
	setAttr ".pm[11]" -type "matrix" 0.70710678118654746 -0 -0.70710678118654757 -0 -0 1 -0 0
		 0.70710678118654757 -0 0.70710678118654746 -0 -0 0 -0 1;
	setAttr ".pm[12]" -type "matrix" 0.92387953251128674 -0 -0.38268343236508973 -0 -0 1 -0 0
		 0.38268343236508973 -0 0.92387953251128674 -0 -0 0 -0 1;
	setAttr ".pm[13]" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
	setAttr ".pm[14]" -type "matrix" 0.92387953251128674 -0 0.38268343236508995 -0 -0 1 -0 0
		 -0.38268343236508995 -0 0.92387953251128674 -0 -0 0 -0 1;
	setAttr ".pm[15]" -type "matrix" 0.70710678118654746 -0 0.70710678118654757 -0 -0 1 -0 0
		 -0.70710678118654757 -0 0.70710678118654746 -0 -0 0 -0 1;
	setAttr ".pm[16]" -type "matrix" 0.38268343236508962 -0 0.92387953251128685 -0 -0 1 -0 0
		 -0.92387953251128685 -0 0.38268343236508962 -0 -0 0 -0 1;
	setAttr ".pm[17]" -type "matrix" 0 -0 -1 -0 -0 1 -0 0 1 -0 0 -0 -0 0 -0 1;
	setAttr ".pm[18]" -type "matrix" 0 -0 1 -0 -0 1 -0 0 -1 -0 0 -0 -0 0 -0 1;
	setAttr ".pm[19]" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
	setAttr ".pm[20]" -type "matrix" 0.38268343236508995 -0 -0.92387953251128685 -0 -0 1 -0 0
		 0.92387953251128685 -0 0.38268343236508995 -0 -0 0 -0 1;
	setAttr ".pm[21]" -type "matrix" 0.70710678118654746 -0 -0.70710678118654757 -0 -0 1 -0 0
		 0.70710678118654757 -0 0.70710678118654746 -0 -0 0 -0 1;
	setAttr ".pm[22]" -type "matrix" 0.92387953251128674 -0 -0.38268343236508973 -0 -0 1 -0 0
		 0.38268343236508973 -0 0.92387953251128674 -0 -0 0 -0 1;
	setAttr ".pm[23]" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
	setAttr ".pm[24]" -type "matrix" 0.92387953251128674 -0 0.38268343236508995 -0 -0 1 -0 0
		 -0.38268343236508995 -0 0.92387953251128674 -0 -0 0 -0 1;
	setAttr ".pm[25]" -type "matrix" 2 -0 0 -0 -0 2 -0 0 0 -0 2 -0 -0 -0.0050000003539025784 -0 1;
	setAttr ".pm[26]" -type "matrix" 0.38268343236508962 -0 0.92387953251128685 -0 -0 1 -0 0
		 -0.92387953251128685 -0 0.38268343236508962 -0 -0 0 -0 1;
	setAttr ".pm[27]" -type "matrix" 0.70710678118654746 -0 0.70710678118654757 -0 -0 1 -0 0
		 -0.70710678118654757 -0 0.70710678118654746 -0 -0 0 -0 1;
	setAttr ".pm[28]" -type "matrix" 0 -0 1 -0 -0 1 -0 0 -1 -0 0 -0 -0 0 -0 1;
	setAttr ".pm[29]" -type "matrix" 0 -0 -1 -0 -0 1 -0 0 1 -0 0 -0 -0 0 -0 1;
	setAttr ".gm" -type "matrix" 0.5 0 0 0 0 0.5 0 0 0 0 0.5 0 0 0.0025000001769512892 0 1;
	setAttr -s 15 ".ma";
	setAttr -s 30 ".dpf[0:29]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 
		4 4 4 4 4 4 4 4 4 4;
	setAttr ".smt[25]"  0;
	setAttr -s 15 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 30;
	setAttr ".ucm" yes;
	setAttr -s 15 ".ifcl";
createNode groupId -n "groupId30";
	rename -uid "D1BCF5E9-4F31-0814-3136-F0BDCB782BBE";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "C7A14ED6-4041-793C-D008-4B91A9AAE141";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:83]";
createNode tweak -n "tweak1";
	rename -uid "353604CF-4C4C-AF9C-C56D-BD9FA82D556E";
createNode objectSet -n "skinCluster1Set";
	rename -uid "9D2AD4B9-4D7B-F5F0-8BE3-A99C7B7CB2CE";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster1GroupId";
	rename -uid "9D796C30-4B16-97C4-62E1-DAA4DF6D6528";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster1GroupParts";
	rename -uid "C363B84F-4800-BCDE-5EF6-78BF255E38CE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet1";
	rename -uid "08673192-4EDD-3315-3178-C3B46606DA9A";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId32";
	rename -uid "8AE63706-46C0-8490-71B9-43B9B0D8A9AC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "16049104-468F-2343-C847-7DB93C546754";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose2";
	rename -uid "C28A1DFA-4117-125B-B218-BCA672AD692B";
	setAttr -s 16 ".wm";
	setAttr ".wm[24]" -type "matrix" 0.5 0 0 0 0 0.5 0 0 0 0 0.5 0 0 0.0025000001769512892 0 1;
	setAttr -s 29 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 -0.39269908169872414 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 -0.70710678118654746 0 0.70710678118654768 1
		 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 -0.78539816339744828 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 -0.70710678118654746 0 0.70710678118654768 1
		 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 -1.1780972450961724 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 -0.70710678118654746 0 0.70710678118654768 1
		 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 -1.5707963267948966 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 -0.70710678118654746 0 0.70710678118654768 1
		 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 -0.39269908169872414 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 6.123233995736766e-17 1 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 -0.78539816339744828 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 6.123233995736766e-17 1 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0 -1.1780972450961724 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 6.123233995736766e-17 1 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 0 -1.1780972450961724 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 0 -0.78539816339744828 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0 -0.39269908169872414 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 0 -1.1780972450961724 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0.70710678118654757 0 0.70710678118654757 1
		 1 1 yes;
	setAttr ".xm[15]" -type "matrix" "xform" 1 1 1 0 -0.78539816339744828 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0.70710678118654757 0 0.70710678118654757 1
		 1 1 yes;
	setAttr ".xm[16]" -type "matrix" "xform" 1 1 1 0 -0.39269908169872414 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0.70710678118654757 0 0.70710678118654757 1
		 1 1 yes;
	setAttr ".xm[17]" -type "matrix" "xform" 1 1 1 0 -1.5707963267948966 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[18]" -type "matrix" "xform" 1 1 1 0 1.5707963267948966 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[19]" -type "matrix" "xform" 1 1 1 0 -1.1780972450961724 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[20]" -type "matrix" "xform" 1 1 1 0 -0.78539816339744828 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[21]" -type "matrix" "xform" 1 1 1 0 -0.39269908169872414 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[22]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[23]" -type "matrix" "xform" 1 1 1 0 -1.1780972450961724 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0.70710678118654757 0 0.70710678118654757 1
		 1 1 yes;
	setAttr ".xm[24]" -type "matrix" "xform" 0.5 0.5 0.5 0 0 0 0 0 0.0025000001769512892
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[25]" -type "matrix" "xform" 1 1 1 0 -0.39269908169872414 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0.70710678118654757 0 0.70710678118654757 1
		 1 1 yes;
	setAttr ".xm[26]" -type "matrix" "xform" 1 1 1 0 -0.78539816339744828 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0.70710678118654757 0 0.70710678118654757 1
		 1 1 yes;
	setAttr ".xm[27]" -type "matrix" "xform" 1 1 1 0 1.5707963267948966 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[28]" -type "matrix" "xform" 1 1 1 0 -1.5707963267948966 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr -s 15 ".m";
	setAttr -s 15 ".p";
	setAttr -s 29 ".g[9:28]" yes no no no no no no no no no no no no no 
		no no no no no no;
	setAttr ".bp" yes;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "6C0FF251-4C77-BC14-79BA-9E878E9DA689";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -1257.1428071884905 -539.20172980350742 ;
	setAttr ".tgi[0].vh" -type "double2" 1263.095187904345 538.40632981112435 ;
select -ne :time1;
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 2 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "Root.s" "Up.is";
connectAttr "Up.s" "Up01.is";
connectAttr "Up.s" "Up02.is";
connectAttr "Up.s" "Up03.is";
connectAttr "Up.s" "Up04.is";
connectAttr "Up.s" "Up05.is";
connectAttr "Up.s" "Up06.is";
connectAttr "Up.s" "Up07.is";
connectAttr "Root.s" "Low.is";
connectAttr "Low.s" "Low01.is";
connectAttr "Low.s" "Low02.is";
connectAttr "Low.s" "Low03.is";
connectAttr "Low.s" "Low04.is";
connectAttr "Low.s" "Low05.is";
connectAttr "Low.s" "Low06.is";
connectAttr "Low.s" "Low07.is";
connectAttr "Root.s" "Left.is";
connectAttr "Root.s" "Right.is";
connectAttr "groupId30.id" "CrepeUpShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "CrepeUpShape.iog.og[0].gco";
connectAttr "skinCluster1GroupId.id" "CrepeUpShape.iog.og[5].gid";
connectAttr "skinCluster1Set.mwc" "CrepeUpShape.iog.og[5].gco";
connectAttr "groupId32.id" "CrepeUpShape.iog.og[6].gid";
connectAttr "tweakSet1.mwc" "CrepeUpShape.iog.og[6].gco";
connectAttr "skinCluster1.og[0]" "CrepeUpShape.i";
connectAttr "tweak1.vl[0].vt[0]" "CrepeUpShape.twl";
connectAttr "groupId29.id" "CrepeBackShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "CrepeBackShape.iog.og[0].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "skinCluster1GroupParts.og" "skinCluster1.ip[0].ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1.ip[0].gi";
connectAttr "bindPose2.msg" "skinCluster1.bp";
connectAttr "Root.wm" "skinCluster1.ma[0]";
connectAttr "Up.wm" "skinCluster1.ma[1]";
connectAttr "Up01.wm" "skinCluster1.ma[2]";
connectAttr "Up02.wm" "skinCluster1.ma[3]";
connectAttr "Up03.wm" "skinCluster1.ma[4]";
connectAttr "Low.wm" "skinCluster1.ma[19]";
connectAttr "Low01.wm" "skinCluster1.ma[20]";
connectAttr "Low02.wm" "skinCluster1.ma[21]";
connectAttr "Low03.wm" "skinCluster1.ma[22]";
connectAttr "Low04.wm" "skinCluster1.ma[23]";
connectAttr "Low05.wm" "skinCluster1.ma[24]";
connectAttr "Low07.wm" "skinCluster1.ma[26]";
connectAttr "Low06.wm" "skinCluster1.ma[27]";
connectAttr "Right.wm" "skinCluster1.ma[28]";
connectAttr "Left.wm" "skinCluster1.ma[29]";
connectAttr "Root.liw" "skinCluster1.lw[0]";
connectAttr "Up.liw" "skinCluster1.lw[1]";
connectAttr "Up01.liw" "skinCluster1.lw[2]";
connectAttr "Up02.liw" "skinCluster1.lw[3]";
connectAttr "Up03.liw" "skinCluster1.lw[4]";
connectAttr "Low.liw" "skinCluster1.lw[19]";
connectAttr "Low01.liw" "skinCluster1.lw[20]";
connectAttr "Low02.liw" "skinCluster1.lw[21]";
connectAttr "Low03.liw" "skinCluster1.lw[22]";
connectAttr "Low04.liw" "skinCluster1.lw[23]";
connectAttr "Low05.liw" "skinCluster1.lw[24]";
connectAttr "Low07.liw" "skinCluster1.lw[26]";
connectAttr "Low06.liw" "skinCluster1.lw[27]";
connectAttr "Right.liw" "skinCluster1.lw[28]";
connectAttr "Left.liw" "skinCluster1.lw[29]";
connectAttr "Root.obcc" "skinCluster1.ifcl[0]";
connectAttr "Up.obcc" "skinCluster1.ifcl[1]";
connectAttr "Up01.obcc" "skinCluster1.ifcl[2]";
connectAttr "Up02.obcc" "skinCluster1.ifcl[3]";
connectAttr "Up03.obcc" "skinCluster1.ifcl[4]";
connectAttr "Low.obcc" "skinCluster1.ifcl[19]";
connectAttr "Low01.obcc" "skinCluster1.ifcl[20]";
connectAttr "Low02.obcc" "skinCluster1.ifcl[21]";
connectAttr "Low03.obcc" "skinCluster1.ifcl[22]";
connectAttr "Low04.obcc" "skinCluster1.ifcl[23]";
connectAttr "Low05.obcc" "skinCluster1.ifcl[24]";
connectAttr "Low07.obcc" "skinCluster1.ifcl[26]";
connectAttr "Low06.obcc" "skinCluster1.ifcl[27]";
connectAttr "Right.obcc" "skinCluster1.ifcl[28]";
connectAttr "Left.obcc" "skinCluster1.ifcl[29]";
connectAttr "CrepeUpShapeOrig.w" "groupParts1.ig";
connectAttr "groupId30.id" "groupParts1.gi";
connectAttr "groupParts3.og" "tweak1.ip[0].ig";
connectAttr "groupId32.id" "tweak1.ip[0].gi";
connectAttr "skinCluster1GroupId.msg" "skinCluster1Set.gn" -na;
connectAttr "CrepeUpShape.iog.og[5]" "skinCluster1Set.dsm" -na;
connectAttr "skinCluster1.msg" "skinCluster1Set.ub[0]";
connectAttr "tweak1.og[0]" "skinCluster1GroupParts.ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1GroupParts.gi";
connectAttr "groupId32.msg" "tweakSet1.gn" -na;
connectAttr "CrepeUpShape.iog.og[6]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "groupParts1.og" "groupParts3.ig";
connectAttr "groupId32.id" "groupParts3.gi";
connectAttr "Root.msg" "bindPose2.m[0]";
connectAttr "Up.msg" "bindPose2.m[1]";
connectAttr "Up01.msg" "bindPose2.m[2]";
connectAttr "Up02.msg" "bindPose2.m[3]";
connectAttr "Up03.msg" "bindPose2.m[4]";
connectAttr "Low.msg" "bindPose2.m[9]";
connectAttr "Low01.msg" "bindPose2.m[19]";
connectAttr "Low02.msg" "bindPose2.m[20]";
connectAttr "Low03.msg" "bindPose2.m[21]";
connectAttr "Low04.msg" "bindPose2.m[22]";
connectAttr "Low05.msg" "bindPose2.m[23]";
connectAttr "Low07.msg" "bindPose2.m[25]";
connectAttr "Low06.msg" "bindPose2.m[26]";
connectAttr "Right.msg" "bindPose2.m[27]";
connectAttr "Left.msg" "bindPose2.m[28]";
connectAttr "bindPose2.w" "bindPose2.p[0]";
connectAttr "bindPose2.m[0]" "bindPose2.p[1]";
connectAttr "bindPose2.m[1]" "bindPose2.p[2]";
connectAttr "bindPose2.m[1]" "bindPose2.p[3]";
connectAttr "bindPose2.m[1]" "bindPose2.p[4]";
connectAttr "bindPose2.m[0]" "bindPose2.p[9]";
connectAttr "bindPose2.m[9]" "bindPose2.p[19]";
connectAttr "bindPose2.m[9]" "bindPose2.p[20]";
connectAttr "bindPose2.m[9]" "bindPose2.p[21]";
connectAttr "bindPose2.m[9]" "bindPose2.p[22]";
connectAttr "bindPose2.m[9]" "bindPose2.p[23]";
connectAttr "bindPose2.m[9]" "bindPose2.p[25]";
connectAttr "bindPose2.m[9]" "bindPose2.p[26]";
connectAttr "bindPose2.m[0]" "bindPose2.p[27]";
connectAttr "bindPose2.m[0]" "bindPose2.p[28]";
connectAttr "Root.bps" "bindPose2.wm[0]";
connectAttr "Up.bps" "bindPose2.wm[1]";
connectAttr "Up01.bps" "bindPose2.wm[2]";
connectAttr "Up02.bps" "bindPose2.wm[3]";
connectAttr "Up03.bps" "bindPose2.wm[4]";
connectAttr "Low.bps" "bindPose2.wm[9]";
connectAttr "Low01.bps" "bindPose2.wm[19]";
connectAttr "Low02.bps" "bindPose2.wm[20]";
connectAttr "Low03.bps" "bindPose2.wm[21]";
connectAttr "Low04.bps" "bindPose2.wm[22]";
connectAttr "Low05.bps" "bindPose2.wm[23]";
connectAttr "Low07.bps" "bindPose2.wm[25]";
connectAttr "Low06.bps" "bindPose2.wm[26]";
connectAttr "Right.bps" "bindPose2.wm[27]";
connectAttr "Left.bps" "bindPose2.wm[28]";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "CrepeBackShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "CrepeUpShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId29.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId30.msg" ":initialShadingGroup.gn" -na;
// End of Crepe.ma
